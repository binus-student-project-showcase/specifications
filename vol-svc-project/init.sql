DROP DATABASE IF EXISTS project_db;

CREATE DATABASE project_db;

USE project_db;

CREATE TABLE `Projects` (
  `project_id` INT NOT NULL AUTO_INCREMENT,
  `project_name` VARCHAR(255) NOT NULL,
  `owner` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`project_id`),
  UNIQUE INDEX `project_id_UNIQUE` (`project_id` ASC) VISIBLE);

